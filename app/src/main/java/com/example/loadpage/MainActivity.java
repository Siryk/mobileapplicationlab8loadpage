package com.example.loadpage;

import android.app.LoaderManager;
import android.content.AsyncTaskLoader;
import android.content.Context;
import android.content.Loader;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class MainActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<String> {
    private static final int LOADER_ID = 1;
    private EditText urlEditText;
    private Spinner protocolSpinner;
    private Button fetchButton;
    private ScrollView contentScrollView;
    private TextView contentTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        urlEditText = findViewById(R.id.urlEditText);
        protocolSpinner = findViewById(R.id.protocolSpinner);
        fetchButton = findViewById(R.id.fetchButton);
        contentScrollView = findViewById(R.id.resultScrollView);
        contentTextView = findViewById(R.id.resultTextView);

        fetchButton.setOnClickListener(v -> fetchWebPage());
    }

    private void fetchWebPage() {
        String url = urlEditText.getText().toString().trim();
        String protocol = protocolSpinner.getSelectedItem().toString();

        if (url.isEmpty()) {
            Toast.makeText(this, "Please enter a URL", Toast.LENGTH_SHORT).show();
            return;
        }

        url = getUrlWithProtocol(url, protocol);

        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            Bundle bundle = new Bundle();
            bundle.putString("url", url);

            LoaderManager loaderManager = getLoaderManager();
            Loader<String> loader = loaderManager.getLoader(LOADER_ID);
            if (loader == null) {
                loaderManager.initLoader(LOADER_ID, bundle, this);
            } else {
                loaderManager.restartLoader(LOADER_ID, bundle, this);
            }
        } else {
            Toast.makeText(this, "Check your internet connection and try again", Toast.LENGTH_SHORT).show();
        }
    }

    private String getUrlWithProtocol(String url, String protocol) {
        if (protocol.equals("HTTP")) {
            return "http://" + url;
        } else if (protocol.equals("HTTPS")) {
            return "https://" + url;
        }
        return url;
    }

    @Override
    public Loader<String> onCreateLoader(int id, Bundle args) {
        return new WebPageLoader(this, args.getString("url"));
    }

    @Override
    public void onLoadFinished(Loader<String> loader, String data) {
        contentTextView.setText(data);
        contentScrollView.smoothScrollTo(0, 0);
    }

    @Override
    public void onLoaderReset(Loader<String> loader) {
        contentTextView.setText("");
    }

    private static class WebPageLoader extends AsyncTaskLoader<String> {
        private final String url;

        public WebPageLoader(Context context, String url) {
            super(context);
            this.url = url;
        }

        @Override
        public String loadInBackground() {
            HttpURLConnection urlConnection = null;

            try {
                URL webpageUrl = new URL(url);
                urlConnection = (HttpURLConnection) webpageUrl.openConnection();
                int responseCode = urlConnection.getResponseCode();

                if (responseCode == HttpURLConnection.HTTP_OK) {
                    InputStream inputStream = urlConnection.getInputStream();
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                    StringBuilder stringBuilder = new StringBuilder();
                    String line;
                    while ((line = bufferedReader.readLine()) != null) {
                        stringBuilder.append(line);
                        stringBuilder.append("\n");
                    }
                    bufferedReader.close();
                    inputStream.close();
                    return stringBuilder.toString();
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
            }

            return null;
        }

        @Override
        protected void onStartLoading() {
            super.onStartLoading();
            forceLoad();
        }
    }
}
